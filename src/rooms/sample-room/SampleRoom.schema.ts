import { Schema, type } from '@colyseus/schema';

export class SampleSchema extends Schema {

  @type('string') mySynchronizedProperty: string = 'Hello world';

}
