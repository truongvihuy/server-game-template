import { Room, Client } from 'colyseus';
import { SampleSchema } from './SampleRoom.schema';

export default class SapmleRoom extends Room<SampleSchema> {

  onCreate (options: any) {
    this.setState(new SampleSchema());

    this.onMessage('type', (client, message) => {
      //
      // handle 'type' message
      //
    });

  }

  onJoin (client: Client, options: any) {
    console.log(client.sessionId, 'joined!');
  }

  onLeave (client: Client, consented: boolean) {
    console.log(client.sessionId, 'left!');
  }

  onDispose() {
    console.log('room', this.roomId, 'disposing...');
  }

}
